package it.texo.max;

import static org.hamcrest.Matchers.hasSize;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(args = "movielist.csv")
@AutoConfigureMockMvc
public class RestServiceApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextLoads() throws Exception {
		assertThat(mockMvc).isNotNull();
	}

	@Test
	public void shouldReturnAllMovies() throws Exception {
		this.mockMvc.perform( get("/movies") )
					.andExpect( status().isOk() )
					.andExpect( jsonPath("$", hasSize(206)) )

					.andExpect( jsonPath("$[19].id").value(20) )
					.andExpect( jsonPath("$[19].year").value(1982) )
					.andExpect( jsonPath("$[19].title").value("The Pirate Movie") )
					.andExpect( jsonPath("$[19].studios").value("20th Century Fox") )
					.andExpect( jsonPath("$[19].producers", hasSize(1)) )
					.andExpect( jsonPath("$[19].producers[0].id").value(20) )
					.andExpect( jsonPath("$[19].producers[0].name").value("David Joseph") )
					.andExpect( jsonPath("$[19].winner").value(false) )

					.andExpect( jsonPath("$[150].id").value(151) )
					.andExpect( jsonPath("$[150].year").value(2009) )
					.andExpect( jsonPath("$[150].title").value("Transformers: Revenge of the Fallen") )
					.andExpect( jsonPath("$[150].studios").value("Paramount Pictures, DreamWorks, Hasbro") )
					.andExpect( jsonPath("$[150].producers", hasSize(4)) )
					.andExpect( jsonPath("$[150].producers[0].id").value(231) )
					.andExpect( jsonPath("$[150].producers[0].name").value("Lorenzo di Bonaventura") )
					.andExpect( jsonPath("$[150].producers[1].id").value(232) )
					.andExpect( jsonPath("$[150].producers[1].name").value("Ian Bryce") )
					.andExpect( jsonPath("$[150].producers[2].id").value(233) )
					.andExpect( jsonPath("$[150].producers[2].name").value("Tom DeSanto") )
					.andExpect( jsonPath("$[150].producers[3].id").value(234) )
					.andExpect( jsonPath("$[150].producers[3].name").value("Don Murphy") )
					.andExpect( jsonPath("$[150].winner").value(true) );
	}

	@Test
	public void shouldReturnOneMovie() throws Exception {
		this.mockMvc.perform( get("/movies/134") )
					.andExpect( status().isOk() )

					.andExpect( jsonPath("$.id").value(134) )
					.andExpect( jsonPath("$.year").value(2005) )
					.andExpect( jsonPath("$.title").value("House of Wax") )
					.andExpect( jsonPath("$.studios").value("Warner Bros., Village Roadshow") )

					.andExpect( jsonPath("$.producers", hasSize(3)) )
					.andExpect( jsonPath("$.producers[0].id").value(195) )
					.andExpect( jsonPath("$.producers[0].name").value("Susan Levin") )
					.andExpect( jsonPath("$.producers[1].id").value(58) )
					.andExpect( jsonPath("$.producers[1].name").value("Joel Silver") )
					.andExpect( jsonPath("$.producers[2].id").value(196) )
					.andExpect( jsonPath("$.producers[2].name").value("Robert Zemeckis") )

					.andExpect( jsonPath("$.winner").value(false) );
	}

	@Test
	public void shouldReturnSearchMovies() throws Exception {
		this.mockMvc.perform( get("/movies?title=rambo") )
					.andExpect( status().isOk() )
					.andExpect( jsonPath("$", hasSize(3)) )

					.andExpect( jsonPath("$[0].id").value(31) )
					.andExpect( jsonPath("$[0].year").value(1985) )
					.andExpect( jsonPath("$[0].title").value("Rambo: First Blood Part II") )
					.andExpect( jsonPath("$[0].studios").value("Columbia Pictures") )
					.andExpect( jsonPath("$[0].producers", hasSize(1)) )
					.andExpect( jsonPath("$[0].producers[0].id").value(32) )
					.andExpect( jsonPath("$[0].producers[0].name").value("Buzz Feitshans") )
					.andExpect( jsonPath("$[0].winner").value(true) )

					.andExpect( jsonPath("$[2].id").value(206) )
					.andExpect( jsonPath("$[2].year").value(2019) )
					.andExpect( jsonPath("$[2].title").value("Rambo: Last Blood") )
					.andExpect( jsonPath("$[2].studios").value("Lionsgate") )
					.andExpect( jsonPath("$[2].producers", hasSize(4)) )
					.andExpect( jsonPath("$[2].winner").value(false) );
	}

	@Test
	public void shouldReturnNotFound() throws Exception {
		this.mockMvc.perform( get("/movies/13400") )
		.andExpect( status().isNotFound() );
	}

	@Test
	public void shouldReturnBadRequest() throws Exception {
		this.mockMvc.perform( get("/movies/1340t") )
					.andExpect( status().isBadRequest() );
	}

	@Test
	public void shouldReturnProducersAwards() throws Exception {
		this.mockMvc.perform( get("/producers/awards") )
					.andExpect( status().isOk() )

					.andExpect( jsonPath("$.min", hasSize(1)) )
					.andExpect( jsonPath("$.min[0].name").value("Joel Silver") )
					.andExpect( jsonPath("$.min[0].interval").value(1) )
					.andExpect( jsonPath("$.min[0].previousWin").value(1990) )
					.andExpect( jsonPath("$.min[0].followingWin").value(1991) )

					.andExpect( jsonPath("$.max", hasSize(1)) )
					.andExpect( jsonPath("$.max[0].name").value("Matthew Vaughn") )
					.andExpect( jsonPath("$.max[0].interval").value(13) )
					.andExpect( jsonPath("$.max[0].previousWin").value(2002) )
					.andExpect( jsonPath("$.max[0].followingWin").value(2015) );
	}

}