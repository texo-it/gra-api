package it.texo.max;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;

import it.texo.max.entity.Movie;
import it.texo.max.entity.Producer;
import it.texo.max.repository.MovieRepository;
import it.texo.max.repository.ProducerRepository;

@Service
public class MovieService implements ApplicationRunner {

    private static final Logger LOGGER = Logger.getLogger(MovieService.class.getName());

    private final MovieRepository movieRepository;
    private final ProducerRepository producerRepository;

    private static final String PATTERN_SPLIT_PRODUCERS = "(\\,\\s?and\\s|\\,|\\sand\\s)";
	private static final char SEPARATOR = ';';
	private static final int SKIP_LINES = 1;

	private Map<MovieKeyTuple, Movie> moviesCache = new HashMap<>();
	private Map<String, Producer> producersCache = new HashMap<>();

    public MovieService(MovieRepository movieRepository, ProducerRepository producerRepository) {
        this.movieRepository = movieRepository;
        this.producerRepository = producerRepository;
    }

    private Movie getMovie(String data[]) {
    	Integer year = Integer.parseInt(data[0]); 
		String title = data[1].trim();

		MovieKeyTuple mkt = new MovieKeyTuple(year, title);
		Movie movie = this.moviesCache.get(mkt);

		if (movie==null) {
			movie = new Movie();
			movie.setYear(year);
			movie.setTitle(title);
			movie.setStudios(data[2]);
			movie.setProducers( this.addProducers(data[3]) );
			movie.setWinner( data[4]!=null && data[4].equals("yes") );
			this.moviesCache.put(mkt, movie);
		}

		return movie;
	}

    private Producer getProducer(String nameField) {
		String name = nameField.trim();
		Producer producer = this.producersCache.get(name);

		if (producer==null) {
			producer = new Producer();
			producer.setName(name);
			producer = producerRepository.save(producer);
			this.producersCache.put(name, producer);
		}

		return producer;
	}

	private List<Producer> addProducers(String producerField) {
		String[] producers = producerField.split(PATTERN_SPLIT_PRODUCERS);

		if (producers==null || producers.length==0)
			return null;

		List<Producer> list = new ArrayList<>();
		for (String nameField : producers) {
			Producer p = this.getProducer(nameField);
			list.add(p);
		}
		return list;
	}

	private List<Movie> parseCSV(Reader fileReader) {
		try {
			CSVReader reader = new CSVReaderBuilder(fileReader)
					.withCSVParser( new CSVParserBuilder().withSeparator(SEPARATOR).build() )
					.withSkipLines( SKIP_LINES )
					.build(); 

			List<Movie> movies = reader.readAll().stream().map(data -> {
				return this.getMovie(data);
			}).collect(Collectors.toList());

			return movies;
		} catch (IOException|CsvException e) {
			e.printStackTrace();
			return null;
		}
	}

    @Override
    public void run(ApplicationArguments args) throws Exception {
    	InputStream input = new FileInputStream(args.getNonOptionArgs().get(0));

		List<Movie> lista = this.parseCSV( new InputStreamReader(input) );

        LOGGER.log(Level.INFO, "Persist: " + lista.size() + " movies");
        movieRepository.saveAll(lista);
        
    }

    private static class MovieKeyTuple {
    	private int year;
    	private String title;

    	public MovieKeyTuple(int year, String title) {
    		this.year = year;
    		this.title = title;
    	}

    	@Override
    	public boolean equals(Object obj) {
    		if (obj instanceof MovieKeyTuple) {
    			MovieKeyTuple mkt = (MovieKeyTuple)obj;
    			return this.year==mkt.year && this.title.equals(mkt.title);
    		}
    		return false;
    	}
    }

}