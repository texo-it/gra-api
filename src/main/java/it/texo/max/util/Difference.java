package it.texo.max.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.util.Pair;

public class Difference {

	private boolean max;
	private int value;
	private List<Pair<Integer, Integer>> pairs;

	public Difference(int[] elements) {
		this(elements, false);
	}

	public Difference(int[] elements, boolean max) {
		this.max = max;
		this.pairs = new ArrayList<>();
		this.value = max ? maxDiff(elements) : minDiff(elements);
	}

	private void addPair(Integer first, Integer second) {
		Pair<Integer, Integer> pair = Pair.of(first, second);
		this.pairs.add(pair);
	}

	private int maxDiff(int[] arr) {
		int diff = 0;

		// Find the max diff by comparing adjacent pairs in sorted array
		for (int i=0; i<arr.length-1; i++) {
			int currentDiff = arr[i + 1] - arr[i];
			if (currentDiff >= diff) {
				if (currentDiff > diff) {
					diff = currentDiff;
					this.pairs.clear();
				}

				this.addPair(arr[i], arr[i + 1]);
			}
		}

		return diff;
	}

	private int minDiff(int[] arr) {
		int diff = Integer.MAX_VALUE;

		// Find the min diff by comparing adjacent pairs in sorted array
		for (int i=0; i<arr.length-1; i++) {
			int currentDiff = arr[i + 1] - arr[i];
			if (currentDiff <= diff) {
				if (currentDiff < diff) {
					diff = currentDiff;
					this.pairs.clear();
				}

				this.addPair(arr[i], arr[i + 1]);
			}
		}

		return diff;
	}

	public boolean isMax() {
		return max;
	}

	public List<Pair<Integer, Integer>> getPairs() {
		return pairs;
	}

	public int getValue() {
		return value;
	}

	@Override
 	public String toString() {
		return "Difference [max=" + max + ", value=" + value + ", pairs=" + pairs + "]";
	}

}