package it.texo.max.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIE")
public class Movie {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_ID", nullable = false)
	private Long id;

	@Column(name = "NU_YEAR", nullable = false)
	private Integer year;

	@Column(name = "NM_TITLE", length = 150, nullable = false)
	private String title;

	@Column(name = "NM_STUDIOS", length = 100)
	private String studios;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
	  name = "MOVIE_PRODUCER",
	  joinColumns = @JoinColumn(name = "CD_MOVIE", referencedColumnName = "CD_ID"),
	  inverseJoinColumns = @JoinColumn(name = "CD_PRODUCER", referencedColumnName = "CD_ID")
	)
	private List<Producer> producers;

	@Column(name = "FG_WINNER", nullable = false)
	private Boolean winner = false;

	public Movie() {
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setStudios(String studios) {
		this.studios = studios;
	}
	public void setProducers(List<Producer> producers) {
		this.producers = producers;
	}
	public void setWinner(Boolean winner) {
		this.winner = winner;
	}

	public Long getId() {
		return id;
	}
	public Integer getYear() {
		return year;
	}
	public String getTitle() {
		return title;
	}
	public String getStudios() {
		return studios;
	}
	public List<Producer> getProducers() {
		return producers;
	}
	public Boolean getWinner() {
		return winner;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Movie) {
			Movie other = (Movie)obj;
			return ( this.title!=null && this.title.equals(other.getTitle()) ) && 
					(this.year!=null && this.year.equals(other.getYear()));
		}
		return false;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", year=" + year + ", title=" + title + ", studios=" + studios + ", producers="
				+ producers + ", winner=" + winner + "]";
	}

}