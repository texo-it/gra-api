package it.texo.max.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import it.texo.max.dto.ProducerDTO;

@NamedNativeQuery(name = "Producer.findWinnersInterval",
	query = "SELECT p.nm_name as name, GROUP_CONCAT(m.nu_year ORDER BY nu_year ASC) as years FROM Producer p JOIN Movie_Producer mp ON mp.cd_producer = p.cd_id JOIN Movie m ON m.cd_id = mp.cd_movie WHERE m.fg_winner = true GROUP BY p.cd_id HAVING count(m.cd_id) > 1",
	resultSetMapping = "Mapping.ProducerDTO")

@SqlResultSetMapping(name = "Mapping.ProducerDTO",
	classes = @ConstructorResult(targetClass = ProducerDTO.class, 
                             columns = {@ColumnResult(name = "name"),
                                        @ColumnResult(name = "years")
                             }))

@Entity
@Table(name = "PRODUCER")
public class Producer {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CD_ID", nullable = false)
	private Long id;

	@Column(name = "NM_NAME", length = 150, nullable = false)
	private String name;

	public Producer() {
	}
	public Producer(String name) {
		this.name = name;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Producer) {
			Producer other = (Producer)obj;
			return this.name!=null && this.name.equals( other.getName() );
		}
		return false;
	}

	@Override
	public String toString() {
		return "Producer [id=" + id + ", name=" + name + "]";
	}

}