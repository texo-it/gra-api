package it.texo.max.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.texo.max.dto.ProducerDTO;
import it.texo.max.entity.Producer;

public interface ProducerRepository extends JpaRepository<Producer, Long> {

	@Query("SELECT producer FROM Producer producer WHERE producer.name = :name")
	public Producer findByName(String name);

	@Query(nativeQuery=true)
	public List<ProducerDTO> findWinnersInterval();

}