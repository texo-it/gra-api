package it.texo.max.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import it.texo.max.entity.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long> {

	@Query("SELECT movie FROM Movie movie WHERE lower(movie.title) LIKE lower(:title)")
    public List<Movie> findByTitle(String title);

}