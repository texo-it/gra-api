package it.texo.max.dto;

import org.springframework.data.util.Pair;

public class WinnerDTO {
	private String name;
	private Integer interval;
	private Integer previousWin;
	private Integer followingWin;

	public WinnerDTO() {
	}
	public WinnerDTO(String name, Integer interval, Integer previousWin, Integer followingWin) {
		this.name = name;
		this.interval = interval;
		this.previousWin = previousWin;
		this.followingWin = followingWin;
	}
	public WinnerDTO(ProducerDTO dto, Integer diff, Pair<Integer, Integer> elements) {
		this(dto.getName(), diff, elements.getFirst(), elements.getSecond());
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public void setPreviousWin(Integer previousWin) {
		this.previousWin = previousWin;
	}
	public void setFollowingWin(Integer followingWin) {
		this.followingWin = followingWin;
	}

	public String getName() {
		return name;
	}
	public Integer getInterval() {
		return interval;
	}
	public Integer getPreviousWin() {
		return previousWin;
	}
	public Integer getFollowingWin() {
		return followingWin;
	}

	@Override
	public String toString() {
		return "WinnerDTO [name=" + name + ", interval=" + interval + ", previousWin=" + previousWin + ", followingWin="
				+ followingWin + "]";
	}

}