package it.texo.max.dto;

import java.util.List;

public class AwardDTO {
	private List<WinnerDTO> min;
	private List<WinnerDTO> max;

	public AwardDTO() {
	}
	public AwardDTO(List<WinnerDTO> min, List<WinnerDTO> max) {
		this.min = min;
		this.max = max;
	}

	public void setMin(List<WinnerDTO> min) {
		this.min = min;
	}
	public void setMax(List<WinnerDTO> max) {
		this.max = max;
	}

	public List<WinnerDTO> getMin() {
		return min;
	}
	public List<WinnerDTO> getMax() {
		return max;
	}

	@Override
	public String toString() {
		return "AwardDTO [min=" + min + ", max=" + max + "]";
	}

}