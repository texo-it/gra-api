package it.texo.max.dto;

public class ProducerDTO {
	
	private static final String SEPARATOR = ",";

	private String name;
	private int[] years;

	public ProducerDTO() {
	}
	public ProducerDTO(String name, String years) {
		this.name = name;
		this.setYears(years);
	}

	public void setName(String name) {
		this.name = name;
	}
	public void setYears(String years) {
		String[] array = years.split(SEPARATOR);
		this.years = new int[array.length];
		for (int i=0; i<array.length; i++) {
			this.years[i] = Integer.parseInt(array[i]);
		}
	}

	public String getName() {
		return name;
	}
	public int[] getYears() {
		return years;
	}

	@Override
	public String toString() {
		return "ProducerDTO [name=" + name + ", years=" + years + "]";
	}

}