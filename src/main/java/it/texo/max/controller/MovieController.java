package it.texo.max.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.texo.max.entity.Movie;
import it.texo.max.exception.NotFoundException;
import it.texo.max.repository.MovieRepository;

@RestController
@RequestMapping("/movies")
public class MovieController {
	
	private final MovieRepository repository;

	private static final Logger LOGGER = Logger.getLogger(MovieController.class.getName());

	public MovieController(MovieRepository repository) {
        this.repository = repository;
    }

	@GetMapping
	public List<Movie> getMovies(@RequestParam(value = "title", defaultValue = "") String title) {
		if (title == null || title.isBlank()) {
			LOGGER.log(Level.INFO, "GET Movies");

			return repository.findAll(); 
		} else {
			LOGGER.log(Level.INFO, "GET Movies: Filter by Title");
			
	        List<Movie> pl = repository.findByTitle("%" + title + "%");
	        return pl;
		}
	}

	@GetMapping("/{id}")
	public Movie getMovieById(@PathVariable Long id) {
		LOGGER.log(Level.INFO, "GET Movie by ID");

		return repository.findById(id).orElseThrow(() -> new NotFoundException());	
	}

}