package it.texo.max.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.texo.max.dto.AwardDTO;
import it.texo.max.dto.ProducerDTO;
import it.texo.max.dto.WinnerDTO;
import it.texo.max.entity.Producer;
import it.texo.max.exception.NotFoundException;
import it.texo.max.repository.ProducerRepository;
import it.texo.max.util.Difference;

@RestController
@RequestMapping("/producers")
public class ProducerController {

	private final ProducerRepository repository;

	private static final Logger LOGGER = Logger.getLogger(ProducerController.class.getName());

	public ProducerController(ProducerRepository repository) {
		this.repository = repository;
	}

	private AwardDTO findAwardsInterval(List<ProducerDTO> producers) {
		Integer min = Integer.MAX_VALUE;
		Integer max = 0;
		List<WinnerDTO> minList = new ArrayList<>();
		List<WinnerDTO> maxList = new ArrayList<>();

		for (ProducerDTO producer : producers) {
			Difference minDiff = new Difference(producer.getYears());
			Difference maxDiff = new Difference(producer.getYears(), true);

			if (minDiff.getValue()<=min) {
				if (minDiff.getValue()<min) {
					minList.clear();
					min = minDiff.getValue();
				}
				for (Pair<Integer, Integer> pair : minDiff.getPairs())
					minList.add( new WinnerDTO(producer, minDiff.getValue(), pair) );
			}

			if (maxDiff.getValue()>=max) {
				if (maxDiff.getValue()>max) {
					maxList.clear();
					max = maxDiff.getValue();
				}
				for (Pair<Integer, Integer> pair : maxDiff.getPairs())
					maxList.add( new WinnerDTO(producer, maxDiff.getValue(), pair) );
			}
		};

		return new AwardDTO(minList, maxList);
	}

	@GetMapping("/awards")
	public AwardDTO getAwards() {
		List<ProducerDTO> producers = repository.findWinnersInterval();
		LOGGER.log(Level.INFO, "Winners: " + producers.size() + " producers");

		return findAwardsInterval(producers);
	}

	@GetMapping
	public List<Producer> getProducers() {
		LOGGER.log(Level.INFO, "GET Producers");

		return repository.findAll(); 
	}

	@GetMapping("/{id}")
	public Producer getProducerById(@PathVariable Long id) {
		LOGGER.log(Level.INFO, "GET Producer by ID");

		return repository.findById(id).orElseThrow(() -> new NotFoundException());
	}

}