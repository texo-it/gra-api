package it.texo.max.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController  {

	@RequestMapping("/error")
	@ResponseBody
	public String handleError(HttpServletRequest request) {
	    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

	    if (status != null) {
	        Integer statusCode = Integer.valueOf(status.toString());

	        if(statusCode == HttpStatus.NOT_FOUND.value()) {
	            return "not found";
	        } else if(statusCode == HttpStatus.BAD_REQUEST.value()) {
	            return "bad request";
	        } else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
	            return "server error";
	        }
	    }
	    return "error (status=" + status + ")";
	}
}