#!/bin/bash

DEFAULT_FILE='movielist.csv'

if [ -z "$1" ]
    then
        FILE=$DEFAULT_FILE
    else
        FILE=$1
fi

echo "$FILE"

./mvnw spring-boot:run -Dspring-boot.run.arguments=$FILE
