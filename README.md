# Golden Raspberry Awards gra-api

API RESTful para leitura da lista de indicados e vencedores da categoria Pior Filme do Golden Raspberry Awards.
Deve listar o(s) produtor(es) com maior intervalo entre dois prêmios consecutivos; e o que obteve dois prêmios mais rápido.

Ao iniciar o serviço, o conteúdo do arquivo _movielist.csv_ será importado automaticamente, porém também é possível iniciar o serviço passando como parâmetro um outro arquivo no mesmo padrão.

O projeto foi desenvolvido em ambiente Linux.

## Inicialização

Para iniciar o serviço pode-se utilizar o script _run.sh_.

**Exemplo:**

1. Execução com importação do arquivo default _movielist.csv_:
`./run.sh`

2. Execução com importação de outro arquivo:
`./run.sh novoarquivo.csv`

## Endpoints

Foram criados alguns endpoints básicos para consulta de filmes e produtores:

**Exemplo:**

1. Listagem de filmes:
`http://localhost:8080/movies`

2. Buscar um filme específico:
`http://localhost:8080/movies/134`

3. Buscar filmes por título:
`http://localhost:8080/movies?title=rambo`

3. Listagem de produtores:
`http://localhost:8080/producers`

## Requisito da API

O **endpoint principal** da API lista o(s) produtor(es) com maior intervalo entre dois prêmios consecutivos, e o que
obteve dois prêmios mais rápido, seguindo o documento de _Especificação do Teste_.

1. Listagem:
`http://localhost:8080/producers/awards`

## Testes automatizados

Os testes automatizados garantem a conformidade do sistema considerando o arquivo de entrada movielist.csv.

Para execução dos testes, pode-se utilizar o seguinte comando:
`./mvnw test`
